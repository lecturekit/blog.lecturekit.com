---
date: 2016-08-27
title: Hello World 🎉
---

Hello it's nice to see you! My name is Henning and this my blog about my project, [Lecture Kit](https://lecturekit.com). I hope you'll find my writings interesting.

***

## Who's behind Lecture Kit? 👀
One guy, actually. My name is Henning and currently I'm studying Information Studies at Aarhus University. I’ve been doing web stuff since the age of 13. I thought it was cool that you could write some lines of code and then your browser would display something completely different from what you were writing in your editor. I was instantly hooked.

In the more recent years I’ve worked with Laravel and Vue, building APIs and single page applications. I completely re-wrote the scandinavian music site’s, [Gaffa](http://gaffa.dk), backend from the ground up using [Lumen](https://lumen.laravel.com/). And afterwards I joined [Timekit](https://www.timekit.io/), where I worked with some great people doing cool stuff.

Today I’m doing my own thing. Building a service making university lectures modern.
