---
date: 2016-08-27T18:24:41+02:00
title: The blog setup 🤓
---

**TL;DR**: The blog is build using [Hugo](https://gohugo.io/) and it's running on [Gitlab Pages](http://pages.gitlab.io/) with [CloudFlare](https://www.cloudflare.com/) as CDN.

Read about the details below.

***

## What's Hugo?
> A Fast and Flexible Static Site Generator built with love in GoLang http://gohugo.io

So, what does that mean? Well, for starters you don't need a database, and you don't need support for PHP or another server side language.
Instead you use markdown files organized in folders which then gets generated as a plain ol' static HTML site.

***

## Gitlab Pages
[Gitlab](https://gitlab.com) is a *free* git repository host. And [Gitlab Pages](https://pages.gitlab.io) allows you to host static pages, also for free! But the greatest thing is, that it actually builds your site - I think this is really cool stuff.

To make this work, you need to create a `.gitlab-ci.yml` file in your repo containing your Hugo site, which looks like this:
```
image: alpine:3.4

before_script:
  - apk update && apk add openssl
  - wget https://github.com/spf13/hugo/releases/download/v0.16/hugo_0.16_linux-64bit.tgz
  - echo "37ee91ab3469afbf7602a091d466dfa5  hugo_0.16_linux-64bit.tgz" | md5sum -c
  - tar xf hugo_0.16_linux-64bit.tgz && cp ./hugo /usr/bin
  - hugo version

test:
  script:
  - hugo
  except:
  - master

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

This tells the Gitlab CI runner to fetch the alpine Docker image and install a few packages and download and install Hugo.

Furthermore the file consist of two jobs, `test` and `pages`.
The test job simply run Hugo which tells whether or not the Hugo site is valid.
And the pages job also run Hugo but it passes the generated site to Gitlab Pages, which then get accessable through your assigned domain.

***

## CloudFlare
And to top it all off I added CloudFlare in front which adds HTTPS/SSL and CDN.
So basically it super charges loading time.
