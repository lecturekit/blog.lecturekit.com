# Test site locally
- Download [Hugo](https://gohugo.io)
- Clone repo
- Go to repo dir and run: `hugo server`

# Add post
- `hugo new post/this-is-my-post.md`
- Commit changes